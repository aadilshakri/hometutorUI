import { BrowserModule,Title} from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FlexLayoutModule} from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MainNavbarComponent } from './components/main-navbar/main-navbar.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { RegisterComponent } from './components/register/register.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { LayoutModule } from '@angular/cdk/layout';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { MatNativeDateModule } from '@angular/material/core';
import { TutorManagementComponent } from './components/tutor-management/tutor-management.component';
import { DataTransferService } from './services/data-transfer.service';
import { TutorService } from './services/tutor.service';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthenticationService} from './services/authentication.service';
import { StudentManagementComponent } from './components/student-management/student-management.component';
import { RegisterStudentComponent } from './components/register-student/register-student.component';
import { RegisterTutorComponent } from './components/register-tutor/register-tutor.component';
import { AuthHtppInterceptorServiceService } from './services/auth-htpp-interceptor-service.service';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './services/loader.interceptor';
import { NgxSpinnerModule } from "ngx-spinner";
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  declarations: [
    AppComponent,
    MainNavbarComponent,
    LandingPageComponent,
    SignInComponent,
    RegisterComponent,
    TutorManagementComponent,
    StudentManagementComponent,
    RegisterStudentComponent,
    RegisterTutorComponent,
    LoaderComponent,
    SpinnerComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatTableModule,
    MatInputModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatSelectModule,
    MatListModule,
    MatSortModule,
    MatIconModule,
    MatDividerModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatCarouselModule,
    MatProgressSpinnerModule,
    NgxSpinnerModule,
    // NgbModule,
    
   

  ],
  providers: [DataTransferService, TutorService,AuthGuardService,AuthenticationService,LoaderService,{ provide:HTTP_INTERCEPTORS, useClass:AuthHtppInterceptorServiceService, multi:true },  { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
    ,Title],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
