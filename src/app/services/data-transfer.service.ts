import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  private messageSource = new BehaviorSubject("Current User");
  sharedMessage = this.messageSource.asObservable();

  constructor() { }

  sendMessage(message: string){
    this.messageSource.next(message);
  }

  getMessage(): Observable<any>{
    return this.messageSource.asObservable();

  }
}
