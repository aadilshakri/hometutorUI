import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TutorService {

  // private baseUrl = "http://localhost:8080/tutor";
  // private baseUrl2 = "http://localhost:8080/student";

  constructor(private http : HttpClient) { }

  getAllTutors(){
    return this.http.get(environment.hometutor+"/tutor/getAllTutors")
  }

  addTutor(tutor){
     return this.http.post(environment.hometutor+"/tutor/addTutor", tutor)
    // .subscribe(resp => {
    //   console.log(resp);
    // });
    
  }

  addStudent(student){
    return this.http.post(environment.hometutor+"/student/addStudent", student)
  }

}
