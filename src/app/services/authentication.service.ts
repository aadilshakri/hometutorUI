import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


export class JwtResponse{
  constructor(
    public jwttoken:string,
     ) {}
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor( private httpClient:HttpClient) { }

  authenticate(emailId, password) {
    console.log(emailId);
    return this.httpClient.post<any>(environment.hometutor+"/login/authenticate",{emailId,password}).pipe(
     map(
       userData => {
        sessionStorage.setItem('emailId',emailId);
        let tokenStr= 'Bearer '+userData.token;
        sessionStorage.setItem('token', tokenStr);
        return userData;
       }
     )
    );
  }


  isUserLoggedIn() {
    let user = sessionStorage.getItem('emailId')
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('emailId')
    sessionStorage.removeItem('token')
  }
}
