import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService,  private router: Router) { }

  ngOnInit() {

    this.spinner.show();
 
    setTimeout(() => {
      this.spinner.hide();
      this.router.navigate(['landingpage']);
    }, 3000);

  }
  
  

}
