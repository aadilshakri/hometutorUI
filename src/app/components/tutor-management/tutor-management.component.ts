import { Component, OnInit } from '@angular/core';
import {DataTransferService} from '../../services/data-transfer.service'
import { TutorService } from 'src/app/services/tutor.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-tutor-management',
  templateUrl: './tutor-management.component.html',
  styleUrls: ['./tutor-management.component.css']
})
export class TutorManagementComponent implements OnInit {

  emailId: string;
  tutors:any;

  constructor(private dataTransferService: DataTransferService, private tutorService: TutorService, private authService:AuthenticationService ) { }

  ngOnInit(): void {

    this.dataTransferService.getMessage().subscribe((message: any) => {this.emailId = message});
    console.log(this.emailId);
    let response=this.tutorService.getAllTutors();
    response.subscribe((data)=>this.tutors=data);
  }

  logout(){
    this.authService.logOut();

  }
}
