import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup,Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {DataTransferService} from '../../services/data-transfer.service'
import { AuthenticationService } from 'src/app/services/authentication.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  emailId: string;
  loginForm:FormGroup;
  password:string;
  invalidLogin = false;
  hide=true;


  constructor(private dataTransferService: DataTransferService, private router: Router, private formBuilder : FormBuilder, private loginservice: AuthenticationService, private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({

      emailId: ['', [Validators.required, Validators.email] ],
      password: ['', [Validators.required, Validators.minLength(8)]]
    })

  }

 
  checkStudentLogin(){
    (this.loginservice.authenticate(this.emailId, this.password).subscribe(
      data => {
        this.router.navigate(['dashboard'])
        this.dataTransferService.sendMessage(this.emailId);
        console.log(this.emailId);
        this.invalidLogin = false
      },
      error => {
        this.invalidLogin = true
        this.openSnackBar();
        console.log("Invalid Credentials");
        this.router.navigate(['login']);
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('emailId');
        
      }
    )
    );


    
  }

  checkTutor(){
    
  }

  openSnackBar( ) {
    this.snackBar.open("Invalid Credentials","Exit", {
      duration: 3000,
    });
  }
  
}
