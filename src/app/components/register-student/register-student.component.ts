import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TutorService } from 'src/app/services/tutor.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PasswordValidation } from '../../password-validator';

@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.css']
})
export class RegisterStudentComponent implements OnInit {

  fullname: string;
  password:string;
  confirmPassword:string;
  hide=true;
  emailId:string;
  age:number;
  registerForm:FormGroup;
  invalidLogin = false;

  constructor(private formBuilder : FormBuilder, private tutorService: TutorService, private snackBar:MatSnackBar) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({

      fullname: ['', [Validators.required] ],
      password: ['', [Validators.required, Validators.minLength(8)]],
      emailId: ['', [Validators.required, Validators.email] ],
      age: ['', [Validators.required] ],
      confirmPassword:['', [Validators.required, Validators.minLength(8)]],
    },{
      validator: PasswordValidation.MatchPassword
    })
  }

  registerStudent(){
    const student = {
      name:this.fullname,
      password:this.password,
      emailId:this.emailId,
      age:this.age,
    };
    console.log(student);
    (this.tutorService.addStudent(student).subscribe(
      data => {
        this.invalidLogin = false
        this.openSnackBar();  
      },
      error => {
        this.invalidLogin = true
        this.openInvalidSnackBar();  
      }
    )
    ); 
  }

  openSnackBar() {
    this.snackBar.open("Registered Sucessfully","Exit", {
      duration: 3000,
    });
  }

  openInvalidSnackBar(){
    this.snackBar.open("Student Already Exist","Exit", {
      duration: 3000,
    });
  }


}
