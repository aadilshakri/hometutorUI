import { Component, OnInit,ChangeDetectionStrategy,ChangeDetectorRef,Inject,OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import {DataTransferService} from '../../services/data-transfer.service';
import {MatCalendar} from '@angular/material/datepicker';
import { TutorService } from 'src/app/services/tutor.service';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PasswordValidation } from '../../password-validator';


@Component({
  selector: 'app-register-tutor',
  templateUrl: './register-tutor.component.html',
  styleUrls: ['./register-tutor.component.css']
})
export class RegisterTutorComponent implements OnInit {

  fullname: string;
  registerForm:FormGroup;
  password:string;
  confirmPassword:string;
  hide=true;
  emailId:string;
  education:string;
  occupation:string;
  subjects: [string];
  invalidLogin = false;

  constructor(private formBuilder : FormBuilder, private tutorService: TutorService, private snackBar:MatSnackBar) { }

  ngOnInit(){
    this.registerForm = this.formBuilder.group({

      fullname: ['', [Validators.required] ],
      password: ['', [Validators.required, Validators.minLength(8)]],
      emailId: ['', [Validators.required, Validators.email] ],
      education: ['', [Validators.required] ],
      occupation: ['', [Validators.required] ],
      subjects: ['', [Validators.required] ],
      confirmPassword:['', [Validators.required, Validators.minLength(8)]],
    },{
      validator: PasswordValidation.MatchPassword
    })
  }

  registerTutor(){
    const tutor = {
      name:this.fullname,
      password:this.password,
      emailId:this.emailId,
      degree:this.education,
      occupation:this.occupation,
      subjects:this.subjects
    };
    console.log(tutor);
    (this.tutorService.addTutor(tutor).subscribe(
      data => {
        this.invalidLogin = false
        this.openSnackBar();  
      },
      error => {
        this.invalidLogin = true
        this.openInvalidSnackBar();  
      }
    )
    );   
  }

  openSnackBar( ) {
    this.snackBar.open("Registered Sucessfully","Exit", {
      duration: 3000,
    });
  }

  openInvalidSnackBar(){
    this.snackBar.open("Tutor Already Exist","Exit", {
      duration: 3000,
    });
  }

}
