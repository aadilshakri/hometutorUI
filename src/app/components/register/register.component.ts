import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup,Validators,FormBuilder,NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {DataTransferService} from '../../services/data-transfer.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  emailId: string;
  registerForm:FormGroup;
  password:string;


  constructor(private dataTransferService: DataTransferService, private router: Router, private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({

      emailId: ['', [Validators.required, Validators.email] ],
      password: ['', [Validators.required, Validators.minLength(8)]]
    })

  }


}
