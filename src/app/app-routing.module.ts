import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainNavbarComponent } from './components/main-navbar/main-navbar.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import {RegisterComponent} from './components/register/register.component';
import{TutorManagementComponent} from './components/tutor-management/tutor-management.component';
import{RegisterTutorComponent} from './components/register-tutor/register-tutor.component';
import{RegisterStudentComponent} from './components/register-student/register-student.component';
import { AuthGuardService } from './services/auth-guard.service';
import {SpinnerComponent} from './components/spinner/spinner.component'

const routes: Routes = [

  {path: '', component: SpinnerComponent},
  {path: 'landingpage', component: LandingPageComponent},
  {path: 'login', component: SignInComponent, data: {title: 'Login'}},
  {path: 'register', component: RegisterComponent,data: {title: 'Register'}},
  {path: 'dashboard', component:TutorManagementComponent,canActivate:[AuthGuardService],data: {title: 'Tutor Dashboard'}},
  {path: 'registerTutor', component: RegisterTutorComponent,data: {title: 'Tutor SignUp'}},
  {path: 'registerStudent', component: RegisterStudentComponent,data: {title: 'Student SignUp'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
